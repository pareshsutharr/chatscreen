import './App.css';
import Header from './mycomponents/Header';
import Footer from './mycomponents/Footer';
import Chat_body from './mycomponents/Chat_body';

function App() {
  return (
    <>
      <Header/>
      <Chat_body/>
      <Footer/>
    </>
  );
}

export default App;
