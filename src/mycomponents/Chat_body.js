import React from 'react'
//chats information array object
const chats =  [
    {
      "id": "4b7dce59ed5e4206baa5f5284950414c",
      "message": "Aenean ullamcorper orci et vulputate fermentum.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Cras erat dui, finibus vel lectus ac, pharetra dictum odio.Duis ac nulla varius diam ultrices rutrum.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/648/160/160.jpg?hmac=AqrvRqv79fFWHWjjjm_Cn7QPPJ2JVox_CLRgzISsO4o",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "a6c04ceed74b447280f5fa7ab053adcc"
      },
      "time": "2023-05-31 10:22:28"
    },
    {
      "id": "dbef0f2d10bc45f9877c5ade4bd7b36f",
      "message": "Cras vel elit sed mi placerat pharetra eget vel odio.Integer ultricies malesuada quam.Quisque vitae varius ex, eu volutpat orci.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/819/160/160.jpg?hmac=duWXAb-022KT3VnXfDCSyr0sLwddRYoP7RMFnidof_g",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "54fbcca87afa4527b28221df348018b1"
      },
      "time": "2023-05-31 10:22:58"
    },
    {
      "id": "9b5d0d69dcdc4144ab777851e6247ba3",
      "message": "Aenean ullamcorper orci et vulputate fermentum.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Cras erat dui, finibus vel lectus ac, pharetra dictum odio.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/648/160/160.jpg?hmac=AqrvRqv79fFWHWjjjm_Cn7QPPJ2JVox_CLRgzISsO4o",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "a6c04ceed74b447280f5fa7ab053adcc"
      },
      "time": "2023-05-31 10:23:28"
    },
    {
      "id": "0a67098d4f9141d6a36412030e2d87cf",
      "message": "Etiam risus sapien, auctor eu volutpat sit amet, porta in nunc.Cras vel elit sed mi placerat pharetra eget vel odio.Nullam tempus scelerisque purus, sed mattis elit condimentum nec.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Cras erat dui, finibus vel lectus ac, pharetra dictum odio.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/819/160/160.jpg?hmac=duWXAb-022KT3VnXfDCSyr0sLwddRYoP7RMFnidof_g",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "54fbcca87afa4527b28221df348018b1"
      },
      "time": "2023-05-31 10:23:58"
    },
    {
      "id": "12044e44f0144e3d90fab7d60b3c6abf",
      "message": "Proin ipsum purus, laoreet quis dictum a, laoreet sed ligula.Duis ac nulla varius diam ultrices rutrum.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/648/160/160.jpg?hmac=AqrvRqv79fFWHWjjjm_Cn7QPPJ2JVox_CLRgzISsO4o",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "a6c04ceed74b447280f5fa7ab053adcc"
      },
      "time": "2023-05-31 10:24:28"
    },
    {
      "id": "caa07079ab8c4f54a386608e546a19d6",
      "message": "Aenean ullamcorper orci et vulputate fermentum.Cras erat dui, finibus vel lectus ac, pharetra dictum odio.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/551/160/160.jpg?hmac=DKAZaW3KPwMLhYwnJ-s_NOYKngMXo-nR1pEQzcNCgr0",
        "is_kyc_verified": true,
        "self": false,
        "user_id": "73785ed67d034f6290b0334c6e756433"
      },
      "time": "2023-05-31 10:24:58"
    },
    {
      "id": "337e2dde08f64887873e38fc970acf28",
      "message": "Aenean ullamcorper orci et vulputate fermentum.Duis ac nulla varius diam ultrices rutrum.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Proin ipsum purus, laoreet quis dictum a, laoreet sed ligula.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/648/160/160.jpg?hmac=AqrvRqv79fFWHWjjjm_Cn7QPPJ2JVox_CLRgzISsO4o",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "a6c04ceed74b447280f5fa7ab053adcc"
      },
      "time": "2023-05-31 10:25:28"
    },
    {
      "id": "9fd523ee92ab4aaeb35d9db2daa35699",
      "message": "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Duis ac nulla varius diam ultrices rutrum.Duis ac nulla varius diam ultrices rutrum.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/551/160/160.jpg?hmac=DKAZaW3KPwMLhYwnJ-s_NOYKngMXo-nR1pEQzcNCgr0",
        "is_kyc_verified": true,
        "self": false,
        "user_id": "73785ed67d034f6290b0334c6e756433"
      },
      "time": "2023-05-31 10:25:58"
    },
    {
      "id": "318156867b334ae798b8d3ac727cc560",
      "message": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nullam tempus scelerisque purus, sed mattis elit condimentum nec.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/819/160/160.jpg?hmac=duWXAb-022KT3VnXfDCSyr0sLwddRYoP7RMFnidof_g",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "54fbcca87afa4527b28221df348018b1"
      },
      "time": "2023-05-31 10:26:28"
    },
    {
      "id": "036751e94ee041159f7d5f230edf6431",
      "message": "Duis ac nulla varius diam ultrices rutrum.Etiam risus sapien, auctor eu volutpat sit amet, porta in nunc.Integer ultricies malesuada quam.",
      "sender": {
        "image": "https://fastly.picsum.photos/id/648/160/160.jpg?hmac=AqrvRqv79fFWHWjjjm_Cn7QPPJ2JVox_CLRgzISsO4o",
        "is_kyc_verified": false,
        "self": false,
        "user_id": "a6c04ceed74b447280f5fa7ab053adcc"
      },
      "time": "2023-05-31 10:26:58"
    }
  ]
  
const Chat_body = () => {
    const arrayDataItems = chats.map(chats => 
      <>
        <li key={chats.id} className='remove-list-style'>
          {chats.sender.is_kyc_verified}  
          <div className="chats-image">
             <img src={chats.sender.image} alt="" srcset="" />
            <div className="text-box">
              {chats.message}
            </div>
          </div>
          </li>
          <li key={chats.id} className='remove-list-style'>
          <div className="chats-image right-side-text">
            <div className="text-box right-side-text-box">
              {chats.message}
            </div>
          </div>
        </li>
        </>
      )
  return (
   <div className='chat-container'>
    <ul>{arrayDataItems}</ul>
   </div>
  )
}

export default Chat_body
